import moment from 'moment'
function Account(user) {
    Object.assign(this, user)
    this.isExpired = function() {
        return this.getTime(new Date()) >= moment(this.expires);
    }
    
    this.getTime = function(d) {
        return moment(d).utc().toDate().getTime() 
    }
    
    return this;
}

export default Account;