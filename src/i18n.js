import Vue from 'vue'
import VueI18n from 'vue-i18n'
import moment from 'moment'

Vue.use(VueI18n)

function loadLocaleMessages () {
  const locales = require.context('./locales', true, /[A-Za-z0-9-_,\s]+\.json$/i)
  const messages = {}
  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i)
    if (matched && matched.length > 1) {
      const locale = matched[1]
      messages[locale] = locales(key)
    }
  })
  return messages
}

const messages = loadLocaleMessages()
const language = window.navigator.userLanguage || window.navigator.language
let currentLocale = language.substring(0, language.indexOf('-')) 
currentLocale = !currentLocale || !(currentLocale in messages) ? (process.env.VUE_APP_I18N_LOCALE || 'en') : currentLocale  
moment.locale(currentLocale);

export default new VueI18n({
  locale: currentLocale,
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en',
  messages
})
