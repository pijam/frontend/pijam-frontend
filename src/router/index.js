import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import Plans from '@/pages/Plans'
import Callback from '@/pages/Callback'
import Secure from '@/pages/Secure'
import Board from '@/pages/Board'
import Application from '@/pages/Application'
import Search from '@/pages/Search'
import UserAccount from '@/pages/UserAccount'
import Register from '@/pages/Register'
import Logout from '@/pages/Logout'
import Account from '../auth/Account'

Vue.use(Router)

async function guard(to, from, next) {
    if ( router.app.$auth.isAuthenticated() ) {
        if ( to.name == 'account' ) {
            next() 
        }
        else if ( to.name == 'secure' ) {
            next({name: 'board'})
        }
        else {
            const user = await router.app.$api.getUser()
            if ( !user ) {
                next({name: 'register'})
            }
            else {
                const account = new Account(user)
                if ( account.isExpired() ) {
                    router.push({name: 'account'})
                }
                else {
                    next()
                }                            
            }
        }
    }
    else {
        router.app.$auth.login(to)
    }
}

const router = new Router({
  mode: 'history',
  routes: [
      {
        path: '/',
        name: 'home',
        component: Home
      },
      {
          path: '/plans',
          name: 'plans',
          component: Plans
      },
      {
        path: '/register',
        name: 'register',
        component: Register,
        beforeEnter: async function (to, from, next) {
            if ( router.app.$auth.isAuthenticated() ) {
                try {
                    const user = await router.app.$api.getUser()
                    if ( user ) {
                        router.app.$logger.debug("/register requested but user exists; pushing /board")
                        next('/secure/board')
                        return;
                    }
                    else {
                        next()
                    }
                }
                catch ( e ) {
                    router.app.$logger.error("Can't fetch user")
                    next('/')
                    return
                }
                next()
            }
            else {
                router.app.$auth.login({name: 'register'})
            }
        }
      },
      {
        path: '/logout',
        name: 'logout',
        component: Logout
      },
      { 
        path: '/secure',
        name: 'secure',
        component: Secure,
        beforeEnter: guard,
        children: [
        {
          path: 'board',
          name: 'board',
          component: Board,
          beforeEnter: guard
        }, 
        {
          path: 'search',
          name: 'search',
          component: Search,
          beforeEnter: guard
        }, 
        {
          path: 'account',
          name: 'account',
          beforeEnter: guard,
          component: UserAccount
        },
        {
            path: 'view',
            name: 'view',
            component: Application,
            beforeEnter(to, from, next) {
                if ( !to.query.id ) {
                    next('/secure/board')
                }
                else {
                    next()
                }
            },
            props: (route) => ({
                id: route.query.id
            })
          }]
    }, 
    {
      path: '/callback',
      name: 'callback',
      component: Callback
    }, 
    {
      path: '*',
      name: '/'
    }
  ]
})

export default router


