
const apiBaseUrl = process.env.NODE_ENV === 'production' 
    ? process.env.VUE_APP_API_BASE_URL
    : process.env.VUE_APP_API_BASE_URL_DEV;

export default {
    api: {
        baseURL: apiBaseUrl || '',
        basePath: '/api/v1/applications'
    },
    trial: {
        duration: 365
    },
    auth: {
        domain: 'pijam.eu.auth0.com',
        clientId: 'ohkdUIwQs6m2ZOeKGbakM68YhlM8mmWQ',
        callbackUrl: window.location.origin + '/callback'
    },
    plans: { 
        //TODO: configure from k8s configmap
         fisher: {
            scopes: 1,
            applicationsPerScope: 10,
            applications: 10
        },
        hunter: {
            scopes: 1,
            applicationsPerScope: 30,
            applications: 50
        },
        sniper: {
            scopes: -1,
            applicationsPerScope: -1,
            applications: -1
        }
    },
}