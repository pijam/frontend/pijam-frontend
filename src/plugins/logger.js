/*
import winston from 'winston'

const logger = winston.createLogger({
    level: 'debug',
    transports: [
        new winston.transports.Console()
    ]
});

if (process.env.NODE_ENV !== 'production') {
    logger.level = 'debug'
    logger.add(new winston.transports.Console({
        format: winston.format.simple()
    }));
}
*/

import tracer from 'tracer'

const logger = tracer.colorConsole({
    format : [
              "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})", //default format
              {
                  error : "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})\nCall Stack:\n{{stack}}" // error format
              }
    ],
    dateformat : "HH:MM:ss.L",
    preprocess :  function(data){
        data.title = data.title.toUpperCase();
    }
});

export default {
  install: function(Vue) {
    Vue.prototype.$logger = logger
  }
};



