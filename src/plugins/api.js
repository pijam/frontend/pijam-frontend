import Vue from 'vue'
import axios from 'axios'
import moment from 'moment'
import config from '../pijam.config.js'

const baseURL = config.api.baseURL
const basePath = config.api.basePath

const client = axios.create({
  baseURL,
  timeout: 12000,
  withCredentials: false
})    

const handleAuth = async function(fn) {
    try {
        return fn()
    }
    catch ( e ) {
        const status = e.response && e.response.status ? e.response.status : -1
        switch ( status ) {
            case 401: 
                api.router.replace({name: 'board'})
                return
            case 403: 
                //TODO handle quota exceeded
                api.router.replace({name: 'account'})
                return
            default:
                throw e
        }
    }
}

const deleteDescendant = async function(resource, id, descendant) {
  if ( !api.$auth.isAuthenticated() ) {
    api.router.replace('/')
    return;
  }
  const token = api.$auth.token;
  await client.delete(`${basePath}/${id}/${resource}s/${descendant.id}`, { 
    headers: { 
      Authorization: `Bearer ${token}`
    }
  })
  api.$logger.debug(`deleted ${resource} ${id}/${descendant.id}`)
};

const createDescendant = async function(resource, id, descendant) {
  if ( !api.$auth.isAuthenticated() ) {
    api.router.replace('/')
    return;
  }
  const token = api.$auth.token;
  const response = await client.post(`${basePath}/${id}/${resource}s`, descendant, { 
    headers: { 
      Authorization: `Bearer ${token}`
    }
  })
  return response.data;
}

const updateDescendant = async function(resource, id, descendant) {
  if ( !api.$auth.isAuthenticated() ) {
    api.router.replace('/')
    return;
  }
  const token = api.$auth.token;
  const response = await client.put(`${basePath}/${id}/${resource}s/${descendant.id}`, descendant, { 
    headers: { 
      Authorization: `Bearer ${token}`
    }
  })
  api.$logger.debug(`updated ${resource} ${id}/${descendant.id}`)
  return response.data;
}

const saveApplication = async function(method, path, data, params) {
  if ( !api.$auth.isAuthenticated() ) {
    api.router.replace('/')
    return;
  }    
  const token = api.$auth.token;
  const response = await client[method](path, data, { 
    params: params || {},  
    headers: { 
      Authorization: `Bearer ${token}`
    }
  })
  api.$logger.debug(`saved application ${response.data.id}`)
  return response.data;
}

const get = async function(path, authenticationRequired, params) {
  return handleAuth(async function() {
    if ( authenticationRequired && !api.$auth.isAuthenticated() ) {
       api.router.replace('/')
       return;
    }
    const token = api.$auth.token;
    const response = await client.get(path, {
      params,
      headers: { 
        Authorization: `Bearer ${token}`
      }
    })
    return response.data;
  })
}

const withBody = async function(method, path, authenticationRequired, data) {
  return handleAuth(async function() {
    if ( authenticationRequired && !api.$auth.isAuthenticated() ) {
       api.router.replace('/')
       return;
    }
    const token = api.$auth.token;
    const response = await client[method](path, data, {
      headers: { 
        Authorization: `Bearer ${token}`
      }
    })
    return response.data;
  })
}

const post = async function(path, authenticationRequired, data) {
  return withBody('post', path, authenticationRequired, data)
}

/* eslint-disable-next-line no-unused-vars */
const put = async function(path, authenticationRequired, data) {
  return withBody('put', path, authenticationRequired, data)
}

const patch = async function(path, authenticationRequired, data) {
  return withBody('patch', path, authenticationRequired, data)
}

const deleteResource = async function(path, authenticationRequired) {
 if ( authenticationRequired && !api.$auth.isAuthenticated() ) {
     api.router.replace('/')
     return;
 }
 const token = api.$auth.token;
 await client.delete(path, {
     headers: { 
         Authorization: `Bearer ${token}`
     }
 })
} 
    
const api =  new Vue({
	data() {
		return { 
			user: null 
		}
	},
    methods: {
        //users
        async getUser() {
            try { 
                return await get(`/api/v1/users`, false)
            }
            catch ( e ) {
                if (e.response && e.response.status == 404) {
                    return null
                }
                throw e
            }
        },
        async createUser(locale) {
            this.user = await post(`/api/v1/users`, false, { locale })
            return this.user
        },
        async deleteUser() {
            await deleteResource(`/api/v1/users`, false)    
        },
        async updateUser(user) {
            this.user = await patch(`/api/v1/users`, false, user)
            return this.user
        },
        logout() {
            get('/api/v1/users/logout', true).then(() => { this.user = null } )
        },
        
        //applications
        async getApplications(params) {
          return (await get(`${basePath}`, true, params));
        },
        getApplicationsAsync(params) {
            return get(`${basePath}`, true, params);
        },
        async getApplication(id) {
          return await get(`${basePath}/${id}`, true)
        },
        async patchApplication(id, data, params) {
          return await saveApplication('patch', `${basePath}/${id}`, data, params)
        },
        async newApplication(data) {
          return await saveApplication('post', `${basePath}`, data)
        },
        async deleteApplication(item) {
            await deleteResource(`${basePath}/${item.id}`, true)
        },
        
        //companies
        async getCompany(id) {
          return await get(`${basePath}/${id}/company`, true)
        },
        
        //notes
        async loadNotes(id) {
          return await get(`${basePath}/${id}/notes`, true);
        },
        async createNote(id) {
          return await createDescendant('note', id, { });
        },
        async updateNote(id, note) {
          return await updateDescendant('note', id, note);
        },
        async deleteNote(id, note) {
            await deleteDescendant('note', id, note)
        },
        
        //interviews
        async loadInterviews(id) {
          return await get(`${basePath}/${id}/interviews`, true);
        },
        async createInterview(id, interview) {
          return await createDescendant('interview', id, Object.assign({}, interview, {
              date: moment(interview.date).format("YYYY-MM-DDTHH:mm:ss.SSSZZ")
          }));
        },
        async updateInterview(id, interview) {
          return await updateDescendant('interview', id, Object.assign({}, interview, {
              date: moment(interview.date).format("YYYY-MM-DDTHH:mm:ss.SSSZZ")
          }))
        },
        async deleteInterview(id, interview) {
            await deleteDescendant('interview', id, interview)
        },
        
        //usage
        async getUsage() {
            return await get(`${basePath}/usage`, true);
        }
    }
})


export default {
  install: function(Vue, opts) {
      api.router = opts.router
      Vue.prototype.$api = api
  }
}