import auth0 from 'auth0-js'
import Vue from 'vue'
import config from '../pijam.config.js'

let webAuth = new auth0.WebAuth({
    domain: config.auth.domain,
    clientID: config.auth.clientId,
    redirectUri: config.auth.callbackUrl,
    responseType: 'token id_token',
    scope: 'openid profile email'
})
let auth = new Vue({
  computed: {
    token: {
      get: function() {
        return localStorage.getItem('id_token')
      },
      set: function(id_token) {
        localStorage.setItem('id_token', id_token)
      }
    },
    accessToken: {
      get: function() {
        return localStorage.getItem('access_token')
      },
      set: function(accessToken) {
        localStorage.setItem('access_token', accessToken)
      }
    },
    expiresAt: {
      get: function() {
        return localStorage.getItem('expires_at')
      },
      set: function(expiresIn) {
        let expiresAt = JSON.stringify(expiresIn * 1000 + new Date().getTime())
        localStorage.setItem('expires_at', expiresAt)
      }
    },
    user: {
      get: function() {
        return JSON.parse(localStorage.getItem('user'))
      },
      set: function(user) {
        localStorage.setItem('user', JSON.stringify(user))
      }
    }
  },
  methods: {
    login(to) {
      localStorage.setItem('redirect_after_login', to ? to.name : "")
      webAuth.authorize({
          prompt: 'login'
      })
    },
    logout() {
      this.$api.logout()
      localStorage.removeItem('access_token')
      localStorage.removeItem('id_token')
      localStorage.removeItem('expires_at')
      localStorage.removeItem('user')
      const loc = window.location
      webAuth.logout({ 
          returnTo: loc.protocol + '//' + loc.host + '/',
          clientID: config.auth.clientId
      })
    },
    isAuthenticated() {
      return new Date().getTime() < localStorage.getItem('expires_at')
    },
    handleAuthentication() {
      return new Promise((resolve, reject) => {  
        webAuth.parseHash((err, authResult) => {
          if (authResult && authResult.accessToken && authResult.idToken) {
            auth.expiresAt = authResult.expiresIn
            auth.accessToken = authResult.accessToken
            auth.token = authResult.idToken
            localStorage.setItem('expiresAt', authResult.expiresIn)
            localStorage.setItem('accessToken', authResult.accessToken)
            localStorage.setItem('token', authResult.idToken)
            localStorage.setItem('user',  JSON.stringify(authResult.idTokenPayload))
            resolve()
          } 
          else if (err) {
            auth.logout()
            reject(err)
          }

        })
      })
    }
  }
})

export default {
  install: function(Vue) {
    Vue.prototype.$auth = auth
  }
}

