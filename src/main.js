require('setimmediate')

import Vue from 'vue'

import VueHead from 'vue-head'

import Vuetify from 'vuetify/lib'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'

import VueTour from 'vue-tour'
require('vue-tour/dist/vue-tour.css')

import router from './router'
import auth from '@/plugins/auth'
import api from '@/plugins/api'
import logger from './plugins/logger.js'
import App from './App'
import AuthMixin from './mixins/auth.js'
import './registerServiceWorker'
import 'vuetify/src/stylus/app.styl'

import VueI18n from 'vue-i18n'
import i18n from './i18n'

Vue.use(VueTour)
Vue.use(VueHead)
Vue.use(VueI18n)
Vue.use(Vuetify, {
    theme: {
        primary: colors.blue,
        secondary: colors.blue.lighten4, 
        accent: colors.amber.accent4
    }
})

Vue.use(auth)
Vue.use(api, { router })
Vue.use(logger)

Vue.mixin(AuthMixin)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  render: h => h(App),
  i18n,
  mounted() {
      if ( this.$auth.isAuthenticated() ) {
          this.$api.getUser().then((u) => {
              if ( u && u.locale ) {
                this.$i18n.locale = u.locale
              }  
          })
      }
  },
  head: {
    title: {
      inner: 'Pijam Is a Job Application Manager'
    },
    meta: [
      { name: 'application-name', content: 'Pijam' },
      { name: 'description', content: 'Job Application Management made easy. Pijam helps you keep track of your job applications and streamline applicatio management.' }, 
      { name: 'charset', content: 'utf-8' },
      { name: 'viewport', content: 'width=device-width,initial-scale=1.0' },
      { name: 'theme-color', content: '#263e52' },
      { name: 'Content-Security-Policy', content: 'upgrade-insecure-requests' }
    ],
    link: [{
        rel: 'stylesheet', 
        href: '//fonts.googleapis.com/css?family=Roboto:400,500,700,400italic|Material+Icons|Comfortaa'
    }, {
        rel: 'preload',
        href: '/resources/css/app.css',
        as: 'style'
    }, {
        rel: 'stylesheet',
        href: '/resources/css/app.css'
    }, {
        rel: 'preload',
        href: '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css',
        as: 'style'
    }, {
        rel: 'stylesheet',
        href: '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css',
    }],
    script: [{
        src: '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js'
    }]
  }
});