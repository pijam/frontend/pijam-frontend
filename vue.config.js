const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
const CompressionPlugin = require('compression-webpack-plugin');
const {GenerateSW} = require('workbox-webpack-plugin');

module.exports = {
  devServer: {
    port: 3000
  },

  configureWebpack: {
    plugins: [
      new VuetifyLoaderPlugin(),
      new CompressionPlugin(),
      new GenerateSW()
    ]
  },

  pwa: {
    name: 'Pijam',
    themeColor: '#2196F3',
    msTileColor: '#fff',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'white',
    workboxPluginMode: 'GenerateSW'
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  }
}

